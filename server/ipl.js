const fs = require('fs').readFile;
const fws = require('fs').writeFile;
fs('/home/siranj/dsp/git/ipl/src/json/mdata.json', 'utf-8', (err, data) => {
  if (err) throw err;
  let obj = JSON.parse(data);
  mpy = {};
  mwpyps = {};
  let season = '';
  let team = '';
  _2k16 = [];
  _2k15 = [];
  // ===================================================//matchesPerYear//=================================================
  for (let i = 0; i < obj.length; i++) {
    season = obj[i].season;
    (mpy.hasOwnProperty(season)) ? mpy[season]++ : mpy[season] = 1;
    if (obj[i].season === 2015) {
      _2k15.push(obj[i].id);
    } if (obj[i].season === 2016) {
      _2k16.push(obj[i].id);
    }
    // ===================================================//matchesPerYear//=================================================

    // ===================================================//matchesWonPerTeam//===============================================
    season = obj[i].season;
    team = obj[i].winner;
    if (mwpyps.hasOwnProperty(season)) {
      (mwpyps[season].hasOwnProperty(team)) ? mwpyps[season][team]++ : mwpyps[season][team] = 1;
    } else {
      mwpyps[season] = {};
    }
  }
  // ===================================================//matchesWonPerTeam//===============================================

  // ===================================================//extraRuns//=======================================================
});
fs('/home/siranj/dsp/git/ipl/src/json/del.json', 'utf-8', (err, data) => {
  if (err) throw err;
  let delData = JSON.parse(data);
  let extras = {};
  let econ = {};
  let arr = [];
  for (let i = 0; i < delData.length; i++) {
    let team = delData[i].bowling_team;
    let id = Number(delData[i].match_id);
    let ext = Number(delData[i].extra_runs);
    let runs = Number(delData[i].batsman_runs) + Number(delData[i].wide_runs) + Number(delData[i].noball_runs);
    let bowlers = delData[i].bowler;
    if ((_2k16.indexOf(id) !== -1)) {
      (extras.hasOwnProperty(team)) ? extras[team] += ext : extras[team] = ext;
    }
    // ===================================================//extraRuns//=======================================================  

    // ===================================================//economy//=======================================================  
    let count = Number(delData[i].ball);
    if ((_2k15.indexOf(id) !== -1)) {
      if (econ.hasOwnProperty(bowlers)) {
        econ[bowlers]['bRuns'] += runs;
        if (count < 7) {
          econ[bowlers]['nBalls']++;
        }
      } else {
        econ[bowlers] = {};
        econ[bowlers]['bRuns'] = runs;
        econ[bowlers]['nBalls'] = 1;
      }
    }
  }
  for (let i in econ) {
    arr.push({
      'bowler': i,
      'economy': Number(((econ[i]['bRuns'] / (econ[i]['nBalls'] / 6))).toFixed(2))
    });
    delete econ[i].nBalls;
    delete econ[i].bRuns;
  }
  arr.sort(function (a, b) {
    return a.economy - b.economy;
  });
  //console.log(mpy);
  //console.log(mwpyps);
  //console.log(extras);
  //console.log(arr.slice(0, 10));

  fws("/home/siranj/dsp/git/ipl/src/output/matchesPerYear.json", JSON.stringify(mpy, null, 4), "utf-8", function (err) {
    if (err) throw err;
  });
  fws("/home/siranj/dsp/git/ipl/src/output/matchesWonPerSeasonPerTeam.json", JSON.stringify(mwpyps, null, 4), "utf-8", function (err) {
    if (err) throw err;
  });
  fws("/home/siranj/dsp/git/ipl/src/output/extrasIn2k16.json", JSON.stringify(extras, null, 4), "utf-8", function (err) {
    if (err) throw err;
  });
  fws("/home/siranj/dsp/git/ipl/src/output/economyIn2k15.json", JSON.stringify(arr, null, 4), "utf-8", function (err) {
    if (err) throw err;
  });
});
